import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });

  // todo: show fighter info (image, name, health, etc.)
  if (fighter) {
    fighterElement.append(createFighterImage(fighter));
    fighterElement.append(createFighterInfo(fighter));
  }

  return fighterElement;
}

export function createFighterInfo(fighter) {
  const { name, health, attack, defense } = fighter;
  const pElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___info'
  });

  pElement.innerHTML = `
    <p><span>Name:</span> ${name}</p>
    <p><span>Health:</span> ${health}</p>
    <p><span>Attack:</span> ${attack}</p>
    <p><span>Defense:</span> ${defense}</p>
  `;

  return pElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes
  });

  return imgElement;
}
