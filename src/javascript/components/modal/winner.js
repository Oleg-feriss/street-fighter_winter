import { createFighterImage } from '../fighterPreview';
import { showModal } from './modal';

export function showWinnerModal(fighter) {
  const modalElements = {
    title: `${fighter.name} WIN!`,
    bodyElement: createFighterImage(fighter)
  };

  showModal(modalElements);

  reloadInterval(2000);
}

function reloadInterval(time) {
  setTimeout(() => {
    location.reload();
  }, time);
}
