import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // keysHandler(() => {
    //   const healthBar = document.getElementById('right-fighter-indicator');
    //   console.log(healthBar);
    //   healthBar.style.width = 50 + '%';
    // }, controls.PlayerOneAttack);

    // resolve the promise with the winner when fight is over
    const firstFighterHp = firstFighter.health;
    const secondFighterHp = secondFighter.health;

    const pressedKeys = new Set();

    document.addEventListener('keydown', (event) => {
      pressedKeys.add(event.code);

      fightProcess(firstFighter, secondFighter, pressedKeys, firstFighterHp, secondFighterHp);

      if (firstFighter.health <= 0) {
        resolve(secondFighter);
      }
      if (secondFighter.health <= 0) {
        resolve(firstFighter);
      }
    });

    document.addEventListener('keyup', (event) => {
      pressedKeys.delete(event.code);
    });
  });
}

function fightProcess(firstFighter, secondFighter, keys, firstHp, secondHp) {
  const firstFighterIndicator = document.getElementById('left-fighter-indicator');
  const secondFighterIndicator = document.getElementById('right-fighter-indicator');

  switch (true) {
    case keys.has(controls.PlayerOneAttack):
      attackFighter(firstFighter, secondFighter, secondFighterIndicator, secondHp, keys);
      break;
    case keys.has(controls.PlayerTwoAttack):
      attackFighter(secondFighter, firstFighter, firstFighterIndicator, firstHp, keys);
      break;
    case controls.PlayerOneCriticalHitCombination.every((key) => keys.has(key)):
      criticalStrike(firstFighter, secondFighter, secondFighterIndicator, secondHp);
      break;
    case controls.PlayerTwoCriticalHitCombination.every((key) => keys.has(key)):
      criticalStrike(secondFighter, firstFighter, firstFighterIndicator, firstHp);
      break;
  }
}

// // function keysHandler(func, ...keys) {
// //   let pressedKeys = new Set();

// //   document.addEventListener('keydown', (event) => {
// //     pressedKeys.add(event.code);

// //     for (let key of keys) {
// //       if (!pressedKeys.has(key)) {
// //         return;
// //       }
// //     }

// //     console.log(isBlocking(keys));

// //     pressedKeys.clear();
// //     func();
// //   });

// //   document.addEventListener('keyup', (event) => {
// //     pressedKeys.delete(event.code);
// //   });
// // }

function attackFighter(attacker, defender, indicator, hp, keys) {
  if (!isBlocking(keys)) {
    const damage = getDamage(attacker, defender);
    defender.health -= damage;
    updateIndicator(defender, indicator, hp);
  } else {
    return 0;
  }
}

function isBlocking(keys) {
  if (keys.has(controls.PlayerOneBlock) || keys.has(controls.PlayerTwoBlock)) {
    return true;
  }
  return false;
}

function updateIndicator(defender, indicator, hp) {
  const currentHealth = defender.health / hp;
  const remains = Math.max(0, currentHealth * 100);
  indicator.style.width = remains + '%';
}

function criticalStrike(attacker, defender, indicator, hp) {
  if (isReadyCriticalStrike(attacker)) {
    defender.health -= attacker.attack * 2;
    attacker.cooldown = new Date();
    updateIndicator(defender, indicator, hp);
  }
}

function isReadyCriticalStrike(fighter) {
  const cooldown = new Date();
  const timeOfCooldown = Math.abs((cooldown - fighter.cooldown) / 1000);
  if (timeOfCooldown < 10) {
    return false;
  }
  return true;
}

// function firstAttack () {
//   const damage = getDamage(attacker, defender);
//     defender.health -= damage;
//     updateindicator(defender, indicator, maxhp);
// }

export function getDamage(attacker, defender) {
  // return damage
  let damageDone = getHitPower(attacker) - getBlockPower(defender);
  return Math.max(0, damageDone);
}

export function getHitPower(fighter) {
  // return hit power
  let criticalHitChance = Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  let dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}
